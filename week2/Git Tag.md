#Git Tag

Git Tag use to tag a commit which mark released version of software.

##How to use it

- Tag listing
```
$ git tag
v0.1
v1.0
```
It also provide a option to list only interested tag.
```
$ git tag -l 'v1.*'
v1.0
v1.1
v1.2
```

- Create a tag

There is two type. First is annotated tag, which can contain additional information and message. Second is lightweight tag, which can't contain additional information and message.

Annotated tag:
```
$ git tag -a v1.0 -m 'released version 1.0'
```

-a -> annotate, value -> a tag

-m -> message, value -> message to note for this tag

Lightweight tag:
```
$ git tag v1.0-lw
```
This can't use -a, -s and -m.

- Show tag information

Annotated tag:
```
$ git show v1.0
tag v1.0
Tagger: Example Example <Example@ex.com>
Date:   Mon Jan 19 11:01:45 2014 +0700

released version 1.0

commit s09df798fdg69sdf9g8sd9f0h8sd09f
Author: Ex2 Example <Example2@ex.com>
Date:   Mon Jan 12 18:47:09 2014 +0700

    complete version 1!
```

Lightweight tag:
```
$ git show v1.0-lw
commit s09df798fdg69sdf9g8sd9f0h8sd09f
Author: Ex2 Example <Example2@ex.com>
Date:   Mon Jan 12 18:47:09 2014 +0700

    complete version 1!
```

- Tag old commit
```
$ git tag -a v1.0 s09df79
```
Specify part of commit checksum at the end of command which can find from "git log --pretty=oneline" or bitbucket

- Push a tag
```
$ git push origin v1.0
```
You need to specify tag to commit. Or you want to push all tag
```
$ git push origin --tags
```

- Checkout a tag
```
$ git checkout -b v1_bug_fixed v1.0
```
This command create a v1_bug_fixed branch in repository with code from v1.0 tag.

- Delete a tag
```
$ git tag -d released_v1.0
```
This command delete tag name "v1.0".


For more option and detail you can go to [Documentation here](http://git-scm.com/docs/git-tag).