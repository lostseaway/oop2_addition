#FeedBack for CoinPure Lab

#Common Mistakes
- Didnt remove //TODO.
- Bad indentation.
- Java file not in coinpurse package!

##5710545015

- Didnt remove //TODO.
- In toString() method in Purse @return missing.
- In line 53 Purse.java } should in new line.

##5710545023

- line 38 in Coin.java. This condition will not happend so it's can be removed.
- Expect @param in equal() method in Coin.java
- Bad indentation in Purse.java

##5710546151

- Does not contain any .java file

##5710546160

- Java file not in coinpurse package!
- Didnt remove //TODO.
- Bad indentation.
- Line 32 in Coin.java. This condition will not happend so it's can be removed.
- No javadoc of every method in Coin.java
- Missing @return in toString() for Purse.java

##5710546178

- Didnt remove //TODO.
- Bad indentation.
- Still remain "@author your name" in javadoc header of Coin.java
- Bad javadoc.
- Missing @return in equals(),toString() of Coin.java
- Bad variable name ex Coins , Coints ????

##5710546186

- Java file not in coinpurse package!
- Didnt remove //TODO.
- Excected @return in toString() of Coin.java , Purse.java
- No javadoc for getValue().
- Bad indentation.

##5710546194

- Please Create a new Repository for StopWatch!
- Java file not in coinpurse package!
- Wrong @param form in equals() , compareTo()
- Expected @return in toString() of Coin.java , Purse.java
- Bad indentation.
- No @auther in Main.java

##5710546208

- Java file not in coinpurse package!
- Wrong @param form in equals() , compareTo()
- No Javadoc for toString() of Coin.java
- Expected "FULL Name" in @author
- Expected @return in toString() of Purse.java
- Didnt remove //TODO.
- Bad indentation.

##5710546216

- Java file not in coinpurse package!
- Bad javadoc. No @param / @return
- Still remain "@author your name" in javadoc header
- Nice indentation!.

##5710546224

- What is "Oh Yeahhhhh" in Commit Msg???
- Bad indentation.
- Don't need to write a duplicate @return.
- No javadoc for toString()
- Expected "FULL Name" in @author
- Didnt remove //TODO.
- Bad variable name in Purse.java ex. CoinList -> coinList

##5710546232

- Bad indentation.
- Wrong @param form in equals() , compareTo()
- No @auther in Main.java
- Didnt remove //TODO.
- Expected @return in toString() of Purse.java

##5710546241

- Should init git in project folder not in the package.
- Nice Indentation!
- No javadoc for toString() in Coin.java
- Duplicate javadoc header in Main.java
- Missing @return of toString() in Purse.java

##5710546259

- Can't found Bitbucket Account

##5710546275

- Wrong @param form in equals() , compareTo()
- No javadoc for toString() in Coin.java , Purse.java
- Bad indentation.
- No @auther in Main.java
- Didnt remove //TODO.

