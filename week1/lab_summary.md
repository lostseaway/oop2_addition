# Lab1: Fundamentals Practice

จากไฟล์ [Lab1-Fundamentals.md](https://bitbucket.org/skeoop/oop/src/2a186258ee033870fd5128cb7fbb338fd6e287cb/week1/Lab1-Fundamentals.md?at=master) อ.ได้บอกไว้ว่าสิ่งที่จะทำใน Lab วันนี้มีอยู่ 3 อย่าง

## 1. การเขียน `equals(Object obj)` ของ Class

- ขั้นแรก Download file [lab1.zip](https://bitbucket.org/skeoop/oop/src/2a186258ee033870fd5128cb7fbb338fd6e287cb/week1/lab1.zip?at=master) แล้วแตกไฟล์ออกมา เราจะได้ไฟล์
	- `Main.java`
	- `Person.java`
	- `Student.java`

ตอนต้นคาบ อ. ได้อธิบายข้อนี้โดยยกตัวอย่างจาก Class

- `Person`

ถ้าหากเราลองเขียน Code

```java
Person a = new Person("Cat");
Person b = new Person("Mouse");
System.out.println(a.equals(b));
Person b = new Person("Cat");
System.out.println(a.equals(b));
```

Output ของ โปรแกรมนี้จะออกมาเป็น
```
false
true
```
ซึ่งถูกต้องแล้ว แต่!!!! ถ้าหากเราแก้Code ด้านบนให้เป็น
```java
Object a = new Person("Cat");
Object b = new Person("Mouse");
System.out.println(a.equals(b));
Object b = new Person("Cat");
System.out.println(a.equals(b));
```
Output ของ โปรแกรมนี้จะเปลี่ยนเป็น
```
false
false
```

คำถามคือทำไมมันถึงเปลี่ยนเป็นเช่นนี้????

สิ่งที่เกิดขึ้นคือ หลังจากที่ทำการเปลี่ยนแปลง Code แล้ว `a.equals(b)` มันไม่ได้เรียกใช้ method `.equals()` จาก Class Person

เราลองมาดูอีกตัวอย่างจาก method `toString()`

```java
Object a = new Person("Cat");
Person b = new Person("Cat");

System.out.println(a.toString());
System.out.println(b.toString());
```

Output
```
Person Cat
Person Cat
```

ผลลัพท์ของ `toString()` ออกมาเหมือนกันเพราะทั้งคู่เรียก `toString()` ของ Class Person เช่นเดียวกัน

