#How to use Git in Terminal / Command line

#Check your system for Git
First, you can check your git in cmd/terminal

- Start Terminal/cmd
- Type the command
> git

The result could be something like :
![alt text](http://i.imgur.com/DSkeNmH.png "Git Example")

Below is full text when you type "git" in terminal/cmd
```
usage: git [--version] [--help] [-C <path>] [-c name=value]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p|--paginate|--no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

The most commonly used git commands are:
   add        Add file contents to the index
   bisect     Find by binary search the change that introduced a bug
   branch     List, create, or delete branches
   checkout   Checkout a branch or paths to the working tree
   clone      Clone a repository into a new directory
   commit     Record changes to the repository
   diff       Show changes between commits, commit and working tree, etc
   fetch      Download objects and refs from another repository
   grep       Print lines matching a pattern
   init       Create an empty Git repository or reinitialize an existing one
   log        Show commit logs
   merge      Join two or more development histories together
   mv         Move or rename a file, a directory, or a symlink
   pull       Fetch from and integrate with another repository or a local branch
   push       Update remote refs along with associated objects
   rebase     Forward-port local commits to the updated upstream head
   reset      Reset current HEAD to the specified state
   rm         Remove files from the working tree and from the index
   show       Show various types of objects
   status     Show the working tree status
   tag        Create, list, delete or verify a tag object signed with GPG

'git help -a' and 'git help -g' lists available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.
```
#Installation
##For Mac / Ubuntu

For ubuntu user if it doesn't show up like this you may need to install git

By typing the command
> sudo apt-get install git

Then finishing the installation.

##For Windows
1. Download Git from [here](http://git-scm.com/download/win) and run the exe file.
2. ![alt text](http://i.imgur.com/q210nCW.png)
3. ![alt text](http://i.imgur.com/H1HXFJU.png)
4. ![alt text](http://i.imgur.com/sIOv0t7.png)
5. ![alt text](http://i.imgur.com/8LtsDn5.png)
6. ![alt text](http://i.imgur.com/ss9X9Hs.png) This page might not show on your installation.
7. ![alt text](http://i.imgur.com/EzY5Oh3.png)
8. ![alt text](http://i.imgur.com/gLtki0k.png) wait wait wait..
9. ![alt text](http://i.imgur.com/c7Up8LJ.png) finish! go check in your cmd as I wrote above.

#Next I will show a example project!

- Go to Document in Terminal
> cd Document

- Create a new folder
> mkdir TestGit

- Then move inside the folder
> cd TestGit

- Create a git repository in local machine
> git init

- Create a file name `readme.md` (using any text editor) with content
```
#Hello Git!!!
```
- Save file in Document/TestGit

- In Terminal type the command
>git add -A

This command mean add every file to your git repository in the local mechine

- Type the command
>git commit -am "add readme.md"

This command mean commit git with commit message "add readme.md"

**Commit = save change of the file that in your repository in the lcoal mechine**

- Create a new repository in bitbucket name "GitTesting" (Don't forget to make it public! uncheck "This is a private repository")

- In your Repository click on "I'm starting from scratch" under "Command line"

- Copy the line that start with `git remote add origin ` and paste in your Terminal. It could be something like :
> git remote add origin https://xxx@bitbucket.org/xxx/testinggit.git

- Push your project to bitbucket by
> git push -u origin master

	- For The first time it will ask for your password

- Refresh your repository it should show up the readme.md

- Next let's modify `readme.md` by adding a line 
```
## This testing for OOP2 Course :)
```

- Save the file

- In Terminal for this time you don't need to use `git add` command because you didn't have any new file just commit the file
> git commit -am "add more readme.md content"

- Then push it to bitbucket
> git push -u origin master

- See on your repository website the `readme.md` should be change!

- Next let's create a new file name `note.txt` with some content

- Save file in the same folder as `readme.md`

- Now you have a new file. You have to add it into your git by
> git add -A

- Then follow by commit it
> git commit -am "add note.txt"

- So push it the bitbucket!
> git push -u origin master

- At the end. See on your repository website `note.txt` should be there :)


